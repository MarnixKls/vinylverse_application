import 'package:flutter/material.dart';

class ColorPalette {
  static const primaryColor = Color.fromARGB(255, 116, 34, 108);
  static const secondaryColor = Color.fromARGB(255, 209, 188, 227);
  static const acceptedColor = Color.fromARGB(255, 121, 201, 158);
  static const deniedColor = Color.fromARGB(255, 236, 78, 32);
}
