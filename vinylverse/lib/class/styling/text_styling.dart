import 'package:flutter/material.dart';

class TextStyling {
  static TextStyle lightTitleLarge =
      TextStyle(fontSize: 30, fontFamily: 'Rubik', color: Colors.grey.shade50);
  static TextStyle darkTitleLarge =
      const TextStyle(fontSize: 30, fontFamily: 'Rubik', color: Colors.black);
  static TextStyle lightTitleSmall =
      TextStyle(fontSize: 22, fontFamily: 'Rubik', color: Colors.grey.shade50);
  static TextStyle darkTitleSmall =
      const TextStyle(fontSize: 22, fontFamily: 'Rubik', color: Colors.black);
  static TextStyle lightBodyLarge = TextStyle(
      fontSize: 26, fontFamily: 'Poppins', color: Colors.grey.shade50);
  static TextStyle darkBodyLarge =
      const TextStyle(fontSize: 26, fontFamily: 'Poppins', color: Colors.black);
  static TextStyle lightBodyMedium = TextStyle(
      fontSize: 20, fontFamily: 'Poppins', color: Colors.grey.shade50);
  static TextStyle darkBodyMedium =
      const TextStyle(fontSize: 20, fontFamily: 'Poppins', color: Colors.black);
  static TextStyle lightBodySmall = TextStyle(
      fontSize: 14, fontFamily: 'Poppins', color: Colors.grey.shade50);
  static TextStyle darkBodySmall =
      const TextStyle(fontSize: 14, fontFamily: 'Poppins', color: Colors.black);
}
