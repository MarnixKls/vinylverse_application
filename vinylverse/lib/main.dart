import 'package:flutter/material.dart';
import 'package:vinylverse/pages/start_page.dart';

void main() {
  runApp(const VinylVerse());
}

class VinylVerse extends StatefulWidget {
  const VinylVerse({super.key});

  @override
  State<VinylVerse> createState() => _VinylVerseState();
}

class _VinylVerseState extends State<VinylVerse> {
  // Root widget of application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Vinyl Verse',
      theme: ThemeData(fontFamily: 'Poppins', primaryColor: Colors.black),
      debugShowCheckedModeBanner: false,
      home: const StartPage(),
    );
  }
}
