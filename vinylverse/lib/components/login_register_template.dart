import 'package:flutter/material.dart';

import '../class/styling/background_painter.dart';
import '../class/styling/color_palette.dart';

class LoginRegisterTemplate extends StatefulWidget {
  final List<Widget> children;
  final double startContainer;
  const LoginRegisterTemplate(
      {super.key, required this.children, required this.startContainer});

  @override
  State<LoginRegisterTemplate> createState() => _LoginRegisterTemplateState();
}

class _LoginRegisterTemplateState extends State<LoginRegisterTemplate> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorPalette.primaryColor,
      body: CustomPaint(
        painter: BackgroundPainter(),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(4.0, 16.0, 8.0, 8.0),
          child: Column(
            children: [
              Flexible(
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: TextButton(
                          onPressed: () => {Navigator.pop(context)},
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.grey.shade50,
                            size: 40.0,
                          )),
                    ),
                    SizedBox(height: widget.startContainer),
                    Align(
                        alignment: Alignment.center,
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.grey.shade50,
                              borderRadius: BorderRadius.circular(18.0),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: const Offset(0, 3),
                                )
                              ]),
                          width: MediaQuery.of(context).size.width / 1.2,
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(children: widget.children),
                          ),
                        )),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
