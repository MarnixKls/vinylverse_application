import 'package:flutter/material.dart';
import 'package:vinylverse/class/styling/color_palette.dart';
import 'package:vinylverse/components/logo_with_text.dart';

class OwnNavigationBar extends StatefulWidget {
  const OwnNavigationBar({super.key});

  @override
  State<OwnNavigationBar> createState() => _OwnNavigationBarState();
}

enum Menu { home, profile, explore }

class _OwnNavigationBarState extends State<OwnNavigationBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: ColorPalette.primaryColor, boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 5,
          blurRadius: 7,
          offset: const Offset(0, 3),
        )
      ]),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 16.0, 0, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            LogoWithText(
              colorVariant: Colors.grey.shade50,
              logoSize: 24.0,
              alignment: MainAxisAlignment.start,
            ),
            PopupMenuButton<Menu>(
                icon: Icon(
                  Icons.menu,
                  color: Colors.grey.shade50,
                ),
                color: ColorPalette.secondaryColor,
                itemBuilder: (BuildContext context) => <PopupMenuEntry<Menu>>[
                      const PopupMenuItem(
                          value: Menu.home,
                          child: ListTile(
                            leading: Icon(Icons.home),
                            title: Text('Home'),
                          )),
                      const PopupMenuItem(
                          value: Menu.profile,
                          child: ListTile(
                            leading: Icon(Icons.person),
                            title: Text('Profile'),
                          )),
                      const PopupMenuItem(
                          value: Menu.explore,
                          child: ListTile(
                            leading: Icon(Icons.explore),
                            title: Text('Explore'),
                          )),
                    ]),
          ],
        ),
      ),
    );
  }
}
