import 'package:flutter/material.dart';

class LogoWithText extends StatefulWidget {
  const LogoWithText(
      {super.key,
      required this.colorVariant,
      required this.logoSize,
      required this.alignment});
  final Color? colorVariant;
  final double logoSize;
  final MainAxisAlignment alignment;
  @override
  State<LogoWithText> createState() => _LogoWithTextState();
}

class _LogoWithTextState extends State<LogoWithText> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: widget.alignment,
      children: [
        SizedBox(
            height: widget.logoSize,
            width: widget.logoSize,
            child: Image.asset(
              'lib/assets/vinyl_collection_icon.png',
              color: widget.colorVariant,
            )),
        SizedBox(
          width: widget.logoSize / 5,
        ),
        Text(
          'VinylVerse',
          style: TextStyle(
              color: widget.colorVariant,
              fontSize: widget.logoSize,
              fontFamily: 'MontSerrat',
              fontWeight: FontWeight.w600),
        )
      ],
    );
  }
}
