import 'package:flutter/material.dart';
import 'package:vinylverse/class/styling/color_palette.dart';
import 'package:vinylverse/components/logo_with_text.dart';
import 'package:vinylverse/pages/register_page.dart';

import '../class/styling/background_painter.dart';
import '../class/styling/text_styling.dart';
import 'login_page.dart';

class StartPage extends StatefulWidget {
  const StartPage({super.key});

  @override
  State<StartPage> createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorPalette.primaryColor,
      body: CustomPaint(
        painter: BackgroundPainter(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Welcome to:',
              style: TextStyling.lightTitleLarge,
            ),
            LogoWithText(
              colorVariant: Colors.grey.shade50,
              logoSize: 35,
              alignment: MainAxisAlignment.center,
            ),
            const SizedBox(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                    onPressed: () => {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const LoginPage()))
                        },
                    child: const Text(
                      'Log in',
                      style: TextStyle(color: ColorPalette.primaryColor),
                    )),
                const SizedBox(
                  width: 20.0,
                ),
                ElevatedButton(
                    onPressed: () => {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const RegisterPage()))
                        },
                    child: const Text(
                      'Sign Up',
                      style: TextStyle(color: ColorPalette.primaryColor),
                    )),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
