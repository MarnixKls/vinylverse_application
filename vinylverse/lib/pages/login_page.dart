import 'package:flutter/material.dart';
import 'package:vinylverse/class/styling/color_palette.dart';
import 'package:vinylverse/class/styling/text_styling.dart';
import 'package:vinylverse/components/login_register_template.dart';
import 'package:vinylverse/components/logo_with_text.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return LoginRegisterTemplate(
      startContainer: MediaQuery.of(context).size.height / 6.0,
      children: [
        const LogoWithText(
            colorVariant: ColorPalette.primaryColor,
            logoSize: 25.0,
            alignment: MainAxisAlignment.end),
        const SizedBox(height: 30.0),
        const Align(
            alignment: Alignment.topLeft,
            child: Text('Welcome back to the VinylVerse!')),
        const SizedBox(height: 10.0),
        TextField(
          decoration: InputDecoration(
              filled: true,
              fillColor: Colors.grey.shade200,
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(16.0)),
              prefixIcon: const Icon(
                Icons.person,
                color: ColorPalette.primaryColor,
              ),
              labelText: 'Username',
              labelStyle: const TextStyle(color: ColorPalette.primaryColor)),
        ),
        const SizedBox(height: 20.0),
        TextField(
          obscureText: true,
          decoration: InputDecoration(
              filled: true,
              fillColor: Colors.grey.shade200,
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(16.0)),
              prefixIcon: const Icon(
                Icons.key,
                color: ColorPalette.primaryColor,
              ),
              labelText: 'Password',
              labelStyle: const TextStyle(color: ColorPalette.primaryColor)),
        ),
        const SizedBox(
          height: 20.0,
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(ColorPalette.secondaryColor),
              ),
              onPressed: () => {},
              child: Wrap(
                children: [
                  Text('Login', style: TextStyling.darkBodyMedium),
                  const SizedBox(width: 8.0),
                  const Icon(
                    Icons.login,
                    color: Colors.black,
                  ),
                ],
              )),
        )
      ],
    );
  }
}
